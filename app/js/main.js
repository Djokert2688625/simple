$(function (){

    const burger = document.querySelector('.header__menu-btn');
    if (burger){
        const body = document.querySelector('.header__menu-list');
        burger.addEventListener('click', function(e){
            $('.header__bottom').toggleClass('lock');
            $('.works').toggleClass('lock');
            $('.service-directions').toggleClass('lock');
            $('.publications').toggleClass('lock');
            $('.presentation').toggleClass('lock');
            $('.footer').toggleClass('lock');
            burger.classList.toggle('active');
            body.classList.toggle('active');
        });
    }

});
